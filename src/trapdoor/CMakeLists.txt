#list all sources in this directory for the library (found in the lib subdirectory)
file(GLOB TRAPDOORLIB_SOURCES "lib/*/*-impl.cpp")

include_directories(${CORE_INCLUDE_DIRS})
include_directories(${PKE_INCLUDE_DIRS})
list(APPEND TRAPDOOR_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/include")
list(APPEND TRAPDOOR_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/lib")
include_directories(${TRAPDOOR_INCLUDE_DIRS})

add_library (trapdoorobj OBJECT ${TRAPDOORLIB_SOURCES})
add_dependencies(trapdoorobj PALISADEcore)
set_property(TARGET trapdoorobj PROPERTY POSITION_INDEPENDENT_CODE 1)

add_library(PALISADEtrapdoor SHARED $<TARGET_OBJECTS:trapdoorobj>)
set_property(TARGET PALISADEtrapdoor PROPERTY VERSION ${TRAPDOOR_VERSION})
set_property(TARGET PALISADEtrapdoor PROPERTY SOVERSION ${TRAPDOOR_VERSION_MAJOR})
set_property(TARGET PALISADEtrapdoor PROPERTY POSITION_INDEPENDENT_CODE 1)


#sets the local destination target for library output
#this creates the lib directory in build and puts PALISADEtrapdoor there
#note without this we would build to build/src/lib
# this line is for non-Windows systems
set_property(TARGET PALISADEtrapdoor PROPERTY LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
# this line is for Windows (MinGW) systems
set_property(TARGET PALISADEtrapdoor PROPERTY RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

install(
	TARGETS PALISADEtrapdoor 
    EXPORT PALISADEtrapdoorTargets 
	DESTINATION lib)
install(
	DIRECTORY include/
	DESTINATION include/palisade-trapdoor)

set (TRAPDOORLIBS PUBLIC PALISADEtrapdoor ${PALISADE_LIBRARIES})

target_link_libraries (PALISADEtrapdoor ${PALISADE_LIBRARIES})

add_custom_target( alltrapdoor ) #special make target to make all trapdoor files
add_dependencies( alltrapdoor PALISADEtrapdoor) #add library to make alltrapdoor target

##########################################################
### now build all files in examples and put executables in bin/trapdoor/examples
##########################################################

set (TRAPDOORAPPS "")
if( BUILD_EXAMPLES ) #set during cmake with -D
  #for all src files in examples directory
  file (GLOB TRAPDOOR_EXAMPLES_SRC_FILES CONFIGURE_DEPENDS examples/*.cpp)
  foreach (app ${TRAPDOOR_EXAMPLES_SRC_FILES})
	get_filename_component ( exe ${app} NAME_WE )
	add_executable ( ${exe} ${app} )
	##this is out output directory under build
	set_property(TARGET ${exe} PROPERTY RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/examples/trapdoor)
	set( TRAPDOORAPPS ${TRAPDOORAPPS} ${exe} ) ##append this apname
	target_link_libraries ( ${exe} ${TRAPDOORLIBS} )
  endforeach()
  
  add_custom_target( alltrapdoorexamples ) #new make target
  add_dependencies( alltrapdoorexamples ${TRAPDOORAPPS} ) #builds all example apps
  add_dependencies( alltrapdoor alltrapdoorexamples ) 
endif()

if( BUILD_UNITTESTS )
	set(UNITTESTMAIN ${PROJECT_SOURCE_DIR}/test/Main_TestAll.cpp)
	file (GLOB TRAPDOOR_TEST_SRC_FILES CONFIGURE_DEPENDS unittest/*.cpp)
	add_executable (trapdoor_tests ${TRAPDOOR_TEST_SRC_FILES} ${UNITTESTMAIN})
	set_property(TARGET trapdoor_tests PROPERTY RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/unittest)
	target_link_libraries ( trapdoor_tests ${TRAPDOORLIBS} )
	if (NOT ${USE_OpenMP} )
		target_link_libraries ( trapdoor_tests PRIVATE Threads::Threads)
	endif()
	add_dependencies( alltrapdoor trapdoor_tests )

	add_custom_command( OUTPUT runtrapdoortests WORKING_DIRECTORY ${CMAKE_BINARY_DIR} COMMAND ${CMAKE_BINARY_DIR}/unittest/trapdoor_tests )
	add_custom_target( testtrapdoor DEPENDS trapdoor_tests runtrapdoortests )
endif()

set( TRAPDOOREXTRAS "" )
if (BUILD_EXTRAS)
	file (GLOB TRAPDOOR_EXTRAS_SRC_FILES CONFIGURE_DEPENDS extras/*.cpp)
	foreach (app ${TRAPDOOR_EXTRAS_SRC_FILES})
		get_filename_component ( exe ${app} NAME_WE )
		add_executable ( ${exe} ${app} )
		set_property(TARGET ${exe} PROPERTY RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/extras/trapdoor)
		set( TRAPDOOREXTRAS ${TRAPDOOREXTRAS} ${exe} )
		target_link_libraries ( ${exe} ${TRAPDOORLIBS} )
	endforeach()

	add_custom_target( alltrapdoorextras )
	add_dependencies( alltrapdoorextras ${TRAPDOOREXTRAS} )
	add_dependencies( alltrapdoor alltrapdoorextras)
endif()

add_custom_command( OUTPUT trapdoorinfocmd COMMAND echo Builds PALISADEtrapdoor and these apps: ${TRAPDOORAPPS} )
add_custom_target( trapdoorinfo DEPENDS trapdoorinfocmd )
